defmodule RepoMinerCore.UserService.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field :handle, :string
    field :name, :string
    field :role, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :handle, :email, :role])
    |> validate_required([:name, :handle, :email, :role])
  end
end
