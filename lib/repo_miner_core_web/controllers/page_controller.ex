defmodule RepoMinerCoreWeb.PageController do
  use RepoMinerCoreWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
